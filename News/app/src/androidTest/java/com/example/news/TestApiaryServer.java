package com.example.news;

import android.content.Context;
import android.content.res.AssetManager;
import android.test.AndroidTestCase;
import android.test.suitebuilder.annotation.LargeTest;
import android.test.suitebuilder.annotation.SmallTest;
import android.util.Log;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.example.news.app.ConnectNetwork;
import com.example.news.app.LoadTextFileFromAssets;

import org.apache.http.client.ResponseHandler;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.BasicResponseHandler;
import org.apache.http.impl.client.DefaultHttpClient;

import java.io.IOException;

/**
 * AP: test ApiaryServer
 * <p>Test connection to network hardware</p>
 * <p>Test connect to apiary server with post and get</p>
 * <p>post with DefaultHttpClient</p>
 * <p>get with volley</p>
 * @author Igor_Kochubey on 7/31/2014.
 */
public class TestApiaryServer extends AndroidTestCase{

    private static final String TAG = "";
    ConnectNetwork mConnect;
    boolean mCurrentConnect;

    static final String URL = "http://kisnews.apiary.io/notes";
    static final String LOCAL_POST = "jsonTestPost.txt";
    static final String LOCAL_GET = "jsonTestGet.txt";

    @Override
    public void setUp() throws Exception {
        super.setUp();
        mConnect = new ConnectNetwork(getContext());
        mCurrentConnect = mConnect.isConnected();
    }

    /**
     * Test connect to network
     * @value mCurrentConnect equals true when wifi or mobile internet is on
     * @see com.example.news.app.ConnectNetwork
     */
    @LargeTest
    public void testConnectNetwork(){
        assertTrue("Disconnect", mCurrentConnect);
    }

    /**
     * Test connect to apiary with post
     * @see com.example.news.app.MainActivity
     */
    @LargeTest
    public void testConnectToApiaryPost() throws IOException{
        if(mCurrentConnect){
            //request to apiary server
            DefaultHttpClient hc = new DefaultHttpClient();
            ResponseHandler<String> res = new BasicResponseHandler();
            HttpPost postMethod = new HttpPost(URL);
            //obtain a response from the server
            String response = hc.execute(postMethod, res);
            //read from assets
            String jsonText = readFromAssets(LOCAL_POST);

            assertEquals("Not equals", jsonText, response);
        }
    }

    /**
     * Test connect to apiary with get volleys
     * @see com.example.news.app.MainActivity
     */
    @LargeTest
    public void testConnectToApiaryGet() throws IOException{
        if(mCurrentConnect){
            //read from assets
            final String jsonText = readFromAssets(LOCAL_GET);

            //read from volley get method
            final Context finalContext = this.getContext();
            RequestQueue requestQueue = Volley.newRequestQueue(finalContext);
            Response.Listener<String> listener = new Response.Listener<String>() {
                @Override
                public void onResponse(String response) {
                    assertEquals("Not equals", jsonText, response);
                }
            };
            Response.ErrorListener errorListener = new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError e) {
                    Log.e(TAG, "error with volley", e);
                }
            };
            StringRequest request = new StringRequest(Request.Method.GET,URL,listener,errorListener);
            requestQueue.add(request);


        }
    }

    /**
     * Test connect to apiary with get HttpDefault
     * @see com.example.news.app.MainActivity
     */
    @LargeTest
    public void testConnectToApiaryHttpDefaultGet() throws IOException{
        if(mCurrentConnect){
            //read from local assets
            String jsonText = readFromAssets(LOCAL_GET);

            //read from get http method
            DefaultHttpClient hc = new DefaultHttpClient();
            ResponseHandler<String> res = new BasicResponseHandler();
            HttpGet getMethod = new HttpGet(URL);
            //obtain a response from the server
            String response = hc.execute(getMethod, res);
            assertEquals("Not equals", jsonText, response);
        }
    }

    /**
     * <p>Read strings from file in assets</p>
     * @param fileName is file in assets
     * @return Strings contains in fileName
     */
    private String readFromAssets(String fileName){
        AssetManager assets = getContext().getAssets();
        LoadTextFileFromAssets loadFromAssets = new LoadTextFileFromAssets(assets);
        return loadFromAssets.getTxt(fileName);
    }
}
