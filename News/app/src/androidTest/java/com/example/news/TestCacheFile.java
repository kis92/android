package com.example.news;

import android.annotation.TargetApi;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Build;
import android.test.AndroidTestCase;
import android.test.suitebuilder.annotation.LargeTest;
import android.test.suitebuilder.annotation.MediumTest;
import com.example.news.app.ImageLoader;
import com.example.news.app.R;

import java.io.File;


/**
 * AP: test CacheFile
 * Test equals Image in assets with image in res/drawable
 * <p>Download image</p>
 * <p>Equals image download and image in assets</p>
 * <p>Delete download image </p>
 * @author Igor_Kochubey on 7/23/2014.
 */
public class TestCacheFile extends AndroidTestCase {

    private File[] mListFiles;
    static final String IMAGE = "http://api.androidhive.info/music/images/adele.png";
    File mCacheDir;


    /**
     * Download image
     */
    @Override
    public void setUp() throws Exception {
        super.setUp();
        //save in SD
//    File mCacheDir = new File(android.os.Environment.getExternalStorageDirectory(), "News");

//save in internal storage "/data/data/com.example.news.app/cache/"
        mCacheDir = new File("/data/data/com.example.news.app/cache/");

        ImageLoader imageLoader = new ImageLoader(getContext().getApplicationContext());
        imageLoader.getBitmap(IMAGE);

        mListFiles = mCacheDir.listFiles();
    }

    /**
     * Test image exists
     */
    @MediumTest
    public void testImageExists(){
        boolean exists = false;
        if (mListFiles.length >= 1)
            exists = true;
        assertTrue(exists);
    }

    /**
     * Test equals Image in assets with image in res/drawable
     */
    @LargeTest
    @TargetApi(Build.VERSION_CODES.KITKAT)
    public void testPicture() {
        ImageLoader imageLoader = new ImageLoader(getContext());
        Bitmap tstImageFromNews = imageLoader.decodeFile(mListFiles[0]);
        Bitmap tstDrawableTrue = BitmapFactory.decodeResource(getContext().getResources(), R.drawable.adele);

        BitmapFactory.Options options = new BitmapFactory.Options();
        options.inJustDecodeBounds = true;
        Bitmap bitmap = BitmapFactory.decodeFile(mListFiles[0].getAbsolutePath(), options);
        if (options.outWidth != -1 && options.outHeight != -1) {
            // This is an image file.
            //Equals
            assertEquals(tstImageFromNews.getHeight(), tstDrawableTrue.getHeight());
            assertEquals(tstImageFromNews.getWidth(), tstDrawableTrue.getWidth());
            assertEquals(tstImageFromNews.getByteCount(), tstDrawableTrue.getByteCount());
            assertEquals(tstImageFromNews.getDensity(), tstDrawableTrue.getDensity());
            assertTrue(tstDrawableTrue.sameAs(tstImageFromNews));

        } else {
            // This is not an image file.
            assertFalse("not image", true);
        }
    }

    /**
     * delete dir and image file
     * @throws Exception
     */
    @Override
    public void tearDown() throws Exception {
        if (mCacheDir.exists()) {
            File img = new File(mCacheDir.getPath() + "/" + mListFiles[0].getName());
            img.delete();
            mCacheDir.delete();
        }
        super.tearDown();
    }

}
