package com.example.news;

/**
 * AP: test MainActivity
 * <p>
 *  Test labels in buttons
 *  Test Buttons in main view
 * </p>
 * @author Igor_Kochubey on 7/22/2014.
 */

import android.annotation.TargetApi;
import android.os.Build;
import android.test.ActivityInstrumentationTestCase2;
import android.test.ViewAsserts;
import android.test.suitebuilder.annotation.SmallTest;
import android.widget.Button;

import com.example.news.app.MainActivity;
import com.example.news.app.R;

public class TestMainActivity extends ActivityInstrumentationTestCase2<MainActivity> {
    public TestMainActivity() {
        super(MainActivity.class);
    }

    private Button mBtnAsyncTask, mBtnLocal, mBtnVolley;

    @Override
    protected void setUp() throws Exception {
        super.setUp();
        MainActivity mMainActivity = (MainActivity) getActivity();

        //initialize
        mBtnAsyncTask = (Button) mMainActivity.findViewById(R.id.btnAsyncTask);
        mBtnLocal = (Button) mMainActivity.findViewById(R.id.btnLocal);
        mBtnVolley = (Button) mMainActivity.findViewById(R.id.btnVolley);
    }

    /**
     * Test labels
     */
    @SmallTest
    public void testButtonsLabel() {
        assertEquals("Incorrect label of the button", "AsyncTask", mBtnAsyncTask.getText());
        assertEquals("Incorrect label of the button", "Local Json", mBtnLocal.getText());
        assertEquals("Incorrect label of the button", "Volley", mBtnVolley.getText());
    }

    /**
     * Test Buttons in main view
     */
    @SmallTest
    @TargetApi(Build.VERSION_CODES.ICE_CREAM_SANDWICH_MR1)
    public void testButtonsInMainView() {
        ViewAsserts.assertOnScreen(mBtnAsyncTask.getRootView(), mBtnAsyncTask);
        ViewAsserts.assertOnScreen(mBtnLocal.getRootView(), mBtnLocal);
        ViewAsserts.assertOnScreen(mBtnVolley.getRootView(), mBtnVolley);
    }

}

