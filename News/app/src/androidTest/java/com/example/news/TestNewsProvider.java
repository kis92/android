package com.example.news;

import android.content.res.AssetManager;
import android.test.AndroidTestCase;
import android.test.suitebuilder.annotation.SmallTest;

import com.example.news.app.LoadTextFileFromAssets;
import com.example.news.app.News;
import com.example.news.app.NewsProvider;

import java.util.ArrayList;

/**
 * AP: test NewsProvider
 * Test mNewsList with TEST_JSON_TXT
 * Test equals news in assets and news in TEST_JSON_TXT
 *
 * @author Igor_Kochubey on 7/24/2014.
 */
public class TestNewsProvider extends AndroidTestCase {

    private static final String LOCAL = "jsonTestLocal.txt";
    private static final String TEST_JSON_TXT =
        "[\n" +
        "{\n" +
        "\"time\":\"21 March 2014\",\n" +
        "\"description\":\"Car: Mancunians and Londoners most likely to be burgled\",\n" +
        "\"image\":\"/assets/cat.jpg\"\n" +
        "},\n" +
        "{\n" +
        "\"time\":\"22 March 2014\",\n" +
        "\"description\":\"Cat: Mancunians and Londoners most likely to be burgled\",\n" +
        "\"image\":\"/assets/ct.png\"\n" +
        "}\n" +
        "]";

    private ArrayList<News> mNewsList, mNewsListAssets;
    private News mNewsFirst, mNewsSecond;

    @Override
    public void setUp() throws Exception {
        super.setUp();

        //initialize
        mNewsList = new NewsProvider().getNewsList(TEST_JSON_TXT);
        mNewsFirst = mNewsList.get(0);
        mNewsSecond = mNewsList.get(1);
    }

    /**
     * <p>test equals news element with TEST_JSON_TXT</p>
     */
    @SmallTest
    public void testNewsProvider() {
        assertEquals(mNewsList.size(), 2);

        assertEquals(mNewsFirst.getDescription(), "Car: Mancunians and Londoners most likely to be burgled");
        assertEquals(mNewsFirst.getImage(), "/assets/cat.jpg");
        assertEquals(mNewsFirst.getTime(), "21 March 2014");

        assertEquals(mNewsSecond.getDescription(), "Cat: Mancunians and Londoners most likely to be burgled");
        assertEquals(mNewsSecond.getImage(), "/assets/ct.png");
        assertEquals(mNewsSecond.getTime(), "22 March 2014");
    }

    /**
     * <p>Test equals news in assets and news in TEST_JSON_TXT</p>
     */
    @SmallTest
    public void testEqualsNews() {
        AssetManager assets = getContext().getAssets();
        LoadTextFileFromAssets loadTxtAssets = new LoadTextFileFromAssets(assets);
        String text = loadTxtAssets.getTxt(LOCAL);

        //initialize
        mNewsListAssets = new NewsProvider().getNewsList(text);
        News newsAssetsFirst = mNewsListAssets.get(0);
        News newsAssetsSecond = mNewsListAssets.get(1);

        assertEquals(newsAssetsFirst, mNewsFirst);
        assertEquals(newsAssetsSecond, mNewsSecond);
    }
}
