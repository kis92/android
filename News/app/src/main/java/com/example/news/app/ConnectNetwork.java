package com.example.news.app;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.util.Log;
import android.widget.Toast;

import java.util.concurrent.ExecutionException;

/**
 * <p>check network connection on hardware</p>
 * <p>check network connection with request</p>
 *
 * @author Igor_Kochubey on 7/23/2014.
 */
public class ConnectNetwork {
    private static final String URL_FOR_TEST = "http://ya.ru/";
    private static final String TAG = "";

    Context context;

    public ConnectNetwork(Context context) {
        this.context = context;
    }

    /**
     * if isConnected equals true then wifi or mobile internet is on
     *
     * @return boolean status
     */
    public boolean isConnected() {
        //hardware
        ConnectivityManager connMgr = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = connMgr.getActiveNetworkInfo();

        boolean connectAndResponse = false;

        //check hardware connect
        if (networkInfo != null && networkInfo.isConnected()) {
            int statusResponse = 0;
            try {
                //get status code response test url
                statusResponse = new ConnectToTestURL().execute(URL_FOR_TEST).get();
            } catch (InterruptedException e) {
                Log.e(TAG, "cannot ConnectToTestURL1", e);
            } catch (ExecutionException e) {
                Log.e(TAG, "cannot ConnectToTestURL2", e);
            }

            if (statusResponse == 200) {
                connectAndResponse = true;
            } else {
                Toast.makeText(context, R.string.nonInternet, Toast.LENGTH_SHORT).show();
            }
        }
        return connectAndResponse;
    }
}
