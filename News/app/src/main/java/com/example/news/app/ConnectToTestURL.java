package com.example.news.app;

import android.os.AsyncTask;
import android.util.Log;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;

/**
 * <p>Connect to test url and return status code</p>
 * @author Igor_Kochubey on 8/1/2014.
 */
public class ConnectToTestURL extends AsyncTask<String, Void, Integer> {
    private static final String TAG = "";

    /**
     * connect to test url and get status
     * @param params - params[0] is test url
     * @return code status
     */
    @Override
    protected Integer doInBackground(String... params) {
        try {
            HttpGet httpRequest = new HttpGet(params[0]);
            HttpClient httpclient = new DefaultHttpClient();
            HttpResponse response = httpclient.execute(httpRequest);
            int code = response.getStatusLine().getStatusCode();
            return code;
        } catch (Exception e) {
            Log.e(TAG, "bad test url", e);
        }
        return -1;
    }

}
