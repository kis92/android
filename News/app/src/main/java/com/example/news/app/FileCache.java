package com.example.news.app;

/**
 * FileCache need for ImageLoader
 * cached downloaded images
 * @author Igor_Kochubey on 03.07.2014.
 */

import java.io.File;

import android.content.Context;

public class FileCache {

    private File cacheDir;

    //save in SD = after delete app, user need delete folder News in sd-card
//    public FileCache(Context context) {
//        //Find the dir to save cached images
//        if (android.os.Environment.getExternalStorageState().equals(android.os.Environment.MEDIA_MOUNTED))
//            cacheDir = new File(android.os.Environment.getExternalStorageDirectory(), "News");
//        else
//            cacheDir = context.getCacheDir();
//        if ((cacheDir != null) && (!cacheDir.exists())) {
//            cacheDir.mkdirs();
//        }
//    }

    //save in internal storage "/data/data/com.example.news.app/cache/"
    public FileCache(Context context) {
        //Find the dir to save cached images
        if (context.getCacheDir().equals(android.os.Environment.getDownloadCacheDirectory()))
            cacheDir = new File(context.getCacheDir(), "News");
        else
            cacheDir = context.getCacheDir();
        if ((cacheDir != null) && (!cacheDir.exists())) {
            cacheDir.mkdirs();
        }
    }

    /**
     * <p>getFile input:fileName; output:contains file</p>
     * @param url - name of file
     * @return file
     */
    public File getFile(String url) {
        String filename = String.valueOf(url.hashCode());
        File file = new File(cacheDir, filename);
        return file;
    }
}
