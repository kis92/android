package com.example.news.app;

import java.util.ArrayList;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

/**
 * @author Igor_Kochubey on 03.07.2014.
 * LazyAdapter
 * find view for TextView and set text
 * find imageView and set images
 * @see com.example.news.app.News
 * @see com.example.news.app.ImageLoader
 */
public class LazyAdapter extends BaseAdapter {

    private ArrayList<News> newsList;
    private static LayoutInflater inflater = null;
    public ImageLoader imageLoader;

    public LazyAdapter(Activity a, ArrayList<News> newsList) {
        this.newsList = newsList;
        inflater = (LayoutInflater) a.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        imageLoader = new ImageLoader(a.getApplicationContext());
    }

    public int getCount() {
        return newsList.size();
    }

    public Object getItem(int position) {
        return position;
    }

    public long getItemId(int position) {
        return position;
    }

    public View getView(int position, View convertView, ViewGroup parent) {
        //initialize elements
        View vi = convertView;
        if (convertView == null) {
            vi = inflater.inflate(R.layout.activity_json, null);
        }
        if (vi != null) {
            TextView time = (TextView) vi.findViewById(R.id.time);
            TextView description = (TextView) vi.findViewById(R.id.description);
            ImageView thumb_image = (ImageView) vi.findViewById(R.id.ivImg);

            News news = newsList.get(position);

            // Setting all values in listview
            time.setText(news.getTime());
            description.setText(news.getDescription());
            imageLoader.DisplayImage(news.getImage(), thumb_image);
        }
        return vi;
    }
}
