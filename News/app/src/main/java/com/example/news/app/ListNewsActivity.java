package com.example.news.app;

import java.util.ArrayList;

import android.app.Activity;
import android.os.Bundle;
import android.widget.ListView;

/**
 * @author Igor_Kochubey on 03.07.2014.
 * this class get parameters (jsonText)
 * and put in Lazy adapter
 * @see com.example.news.app.LazyAdapter
 * @see com.example.news.app.NewsProvider
 */

public class ListNewsActivity extends Activity {

    public static String jsonText;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.url);
        //get parameters in mainActivity
        Bundle extras = getIntent().getExtras();
        //set to string for parse
        if (extras != null) {
            String jsontext = extras.getString(jsonText);
            ArrayList<News> newsList = new NewsProvider().getNewsList(jsontext);
            ListView list = (ListView) findViewById(R.id.list);

            // Getting adapter by passing json data ArrayList
            LazyAdapter adapter = new LazyAdapter(this, newsList);
            list.setAdapter(adapter);
        }
    }
}
