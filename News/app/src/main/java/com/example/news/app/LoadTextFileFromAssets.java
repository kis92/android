package com.example.news.app;

import android.content.res.AssetManager;
import android.util.Log;

import java.io.IOException;
import java.io.InputStream;

/**
 * @author Igor_Kochubey on 7/23/2014.
 * Load text file from Assets
 */
public class LoadTextFileFromAssets {
    private static final String TAG = "";
    private AssetManager assets;

    public LoadTextFileFromAssets(AssetManager assets) {
        this.assets = assets;
    }

    /**
     * <p>read text from file in assets</p>
     * @param str - path to file
     * @return contains file
     */
    public String getTxt(String str) {
        String jsontext = "";
        try {
            InputStream is = assets.open(str);
            byte[] buffer = new byte[is.available()];
            is.read(buffer);
            is.close();
            jsontext = new String(buffer);
            return jsontext;
        } catch (IOException e) {
            Log.e(TAG, "error for loading file in assets", e);
        }
        return jsontext;
    }


}
