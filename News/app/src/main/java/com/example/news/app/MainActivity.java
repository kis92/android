package com.example.news.app;

import android.annotation.TargetApi;
import android.app.Activity;
import android.content.Context;
import android.content.res.AssetManager;
//import android.support.v7.app.ActionBarActivity;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.widget.Button;

import android.content.Intent;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

/**
 * @author Igor_Kochubey on 01.07.2014.
 * MainActivity class
 * has a three button:
 * btnAsyncTask for post
 * btnLocal for assets
 * btnVolley for get with volley lib
 * <p/>
 * private:
 * String getTxt(String)
 * boolean isConnected()
 * <p/>
 * URL is web address to json request
 * LOCAL is name json-file in assets
 * TAG for logging
 */

public class MainActivity extends Activity {
    private static final String TAG = "";
    static final String URL = "http://kisnews.apiary.io/notes";
    static final String LOCAL = "jsonLocal.txt";

    @Override
    public void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Button btnAsyncTask = (Button) findViewById(R.id.btnAsyncTask);
        Button btnLocal = (Button) findViewById(R.id.btnLocal);
        Button btnVolley = (Button) findViewById(R.id.btnVolley);

        final ConnectNetwork connect = new ConnectNetwork(this);

        //connect with AsyncTask
        btnAsyncTask.setOnClickListener(new OnClickListener() {
            @TargetApi(Build.VERSION_CODES.CUPCAKE)
            @Override
            public void onClick(View v) {
                if (connect.isConnected()) {
                    Toast.makeText(getBaseContext(), R.string.connect, Toast.LENGTH_SHORT).show();
                    new RequestTask(MainActivity.this, ListNewsActivity.class).execute(URL);
                } else {
                    Toast.makeText(getBaseContext(), R.string.getInternet, Toast.LENGTH_SHORT).show();
                }
            }
        });

        //read local file in assets
        btnLocal.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                AssetManager assets = getAssets();
                LoadTextFileFromAssets loadTxtAsts = new LoadTextFileFromAssets(assets);
                Intent intent = new Intent(MainActivity.this, ListNewsActivity.class);
                String jsontext = loadTxtAsts.getTxt(LOCAL);
                intent.putExtra(ListNewsActivity.jsonText, jsontext);
                startActivity(intent);
            }
        });

        //connect with volley
        final Context finalContext = this;
        btnVolley.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                if (connect.isConnected()) {
                    Toast.makeText(getBaseContext(), R.string.connect, Toast.LENGTH_SHORT).show();
                    RequestQueue reqQueue = Volley.newRequestQueue(finalContext);
                    StringRequest strReq = new StringRequest(Request.Method.GET, URL, new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {
                            Intent intent = new Intent(MainActivity.this, ListNewsActivity.class);
                            intent.putExtra(ListNewsActivity.jsonText, response);
                            startActivity(intent);
                        }
                    }, new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError e) {
                            Log.e(TAG, "error with volley", e);
                        }
                    });
                    reqQueue.add(strReq);
                } else {
                    Toast.makeText(getBaseContext(), R.string.getInternet, Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

}

