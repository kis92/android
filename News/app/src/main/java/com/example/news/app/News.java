package com.example.news.app;

/**
 * @author Igor_Kochubey on 03.07.2014.
 *
 * News pojo and equals news
 * class News have three Strings:
 * time, description and path to image(url or assets)
 */

public class News {

    private String time = "time";
    private String description = "description";
    private String image = "image";

    public News(String time, String description, String image) {
        this.time = time;
        this.description = description;
        this.image = image;
    }

    public String getTime() {
        return time;
    }

    public String getDescription() {
        return description;
    }

    public String getImage() {
        return image;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;

        News other = (News) obj;
        if (!time.equals(other.time))
            return false;
        if (!description.equals(other.description))
            return false;
        if (!image.equals(other.image))
            return false;
        return true;
    }
}

