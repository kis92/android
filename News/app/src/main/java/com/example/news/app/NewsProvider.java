package com.example.news.app;

import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;

import java.util.ArrayList;

/**
 * @author Igor_Kochubey on 7/23/2014.
 * @see com.example.news.app.News
 * @see com.example.news.app.LazyAdapter
 * <p>parsing jsonText in  newsList</p>
 */
public class NewsProvider {

    public NewsProvider() {
    }

    private static final String TAG = "";

    /**
     * <p>fill news</p>
     * @param jsontext - read downloaded text in file
     * @return ArrayList news
     */
    public ArrayList<News> getNewsList(String jsontext) {
        ArrayList<News> newsList = new ArrayList<News>();
        try {
            JSONArray urls = new JSONArray(jsontext);
            for (int i = 0; i < urls.length(); i++) {
                News news = new News(urls.getJSONObject(i).getString("time").toString(),
                        urls.getJSONObject(i).getString("description").toString(),
                        urls.getJSONObject(i).getString("image").toString()
                );
                newsList.add(news);
            }
        } catch (JSONException e) {
            Log.e(TAG, "Error parsing json format ", e);
        }
        return newsList;
    }

}
