package com.example.news.app;

import android.app.Activity;
import android.content.Intent;
import android.os.AsyncTask;
import android.util.Log;

import org.apache.http.client.ResponseHandler;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.BasicResponseHandler;
import org.apache.http.impl.client.DefaultHttpClient;

/**
 * @author Igor_Kochubey on 7/23/2014.
 * @see com.example.news.app.MainActivity
 * <p>download json text and put in intent json text</p>
 */
public class RequestTask extends AsyncTask<String, String, String> {

    private static final String TAG = "";
    Activity start;
    Class finish;

    public RequestTask(Activity start, Class finish) {
        this.start = start;
        this.finish = finish;
    }

    @Override
    protected String doInBackground(String... params) {
        try {
            //create a request (post) to the server
            DefaultHttpClient hc = new DefaultHttpClient();
            ResponseHandler<String> res = new BasicResponseHandler();
            HttpPost postMethod = new HttpPost(params[0]);
            //obtain a response from the server
            String response = hc.execute(postMethod, res);
            Intent intent = new Intent(start, finish);
            intent.putExtra(ListNewsActivity.jsonText, response);
            start.startActivity(intent);
        } catch (Exception e) {
            Log.e(TAG, "error for loading with AsyncTask", e);
        }
        return null;
    }

}
